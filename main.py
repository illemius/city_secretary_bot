import asyncio
import logging

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.types import ChatType
from aiogram.utils import context, exceptions, executor

import checker
import config
import models

logging.basicConfig(level=logging.DEBUG)

DIALOG_KEY = 'DIALOG'

loop = asyncio.get_event_loop()
bot = Bot(token=config.TOKEN)
dp = Dispatcher(bot)


async def on_start(*_):
    dp.register_message_handler(cmd_watch,
                                commands=['watch'],
                                func=admin_of_dialog)
    dp.register_message_handler(cmd_unwatch,
                                commands=['unwatch'],
                                func=admin_of_dialog)
    dp.register_message_handler(handle_messages,
                                func=watchable)


async def on_stop(*_):
    pass


def watchable(message: types.Message):
    if message.chat.type == ChatType.PRIVATE:
        return False
    try:
        dialog = models.Dialog.get(chat_id=message.chat.id)
    except models.Dialog.DoesNotExist:
        return False

    context.set_value(DIALOG_KEY, dialog)
    return True


async def admin_of_dialog(message: types.Message):
    if message.chat.type == ChatType.PRIVATE:
        return False

    member = await bot.get_chat_member(message.chat.id, message.from_user.id)
    if member.status not in ['administrator', 'creator']:
        return False

    return True


async def handle_messages(message: types.Message):
    dialog = context.get_value(DIALOG_KEY)
    await checker.check_message(message, dialog)


async def cmd_watch(message: types.Message):
    target = message.get_args()

    try:
        target = int(target)
        # chat = await bot.get_chat(target)
        # if chat.type != ChatType.CHANNEL:
        #     return False
    except (ValueError, exceptions.TelegramAPIError):
        return False

    dialog, _ = models.Dialog.get_or_create(chat_id=message.chat.id, target=target)

    await message.reply('Принято.')
    await message.delete()


async def cmd_unwatch(message: types.Message):
    try:
        dialog = models.Dialog.get(chat_id=message.chat.id)
    except models.Dialog.DoesNotExist:
        return False

    dialog.unwatch()
    await message.reply('Принято.')


def main():
    loop.set_task_factory(context.task_factory)
    models.setup_db()
    executor.start_pooling(dp, loop=loop, on_startup=on_start, on_shutdown=on_stop)


if __name__ == '__main__':
    main()
