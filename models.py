from playhouse.postgres_ext import *

import config

db = PostgresqlDatabase(**config.DATABASE)


class BaseModel(Model):
    class Meta:
        database = db


class Dialog(BaseModel):
    chat_id = BigIntegerField()
    target = BigIntegerField()

    def unwatch(self):
        return self.delete().where(type(self).chat_id == self.chat_id).execute()


def setup_db():
    db.create_tables([
        Dialog
    ], True)
