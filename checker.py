from aiogram import types
from aiogram.types import ParseMode

import config
import models


def is_profile(text):
    return 'До битвы осталось' in text and 'Состояние' in text


def is_report(text):
    return '@CityWarsReports' in text and 'Цель' in text


async def check_message(message: types.Message, dialog: models.Dialog):
    if message.forward_from is None:
        return False
    if message.forward_from.id != config.CITY_WARS_BOT:
        return False

    if is_profile(message.text):
        await message.bot.send_message(
            message.chat.id,
            f"{message.from_user.get_mention(as_html=True)}, запрещено отправлять свой профиль в этот диалог!",
            parse_mode=ParseMode.HTML
        )
        await message.delete()
    elif is_report(message.text):
        await message.bot.send_message(
            dialog.target,
            f"Пользователь: {message.from_user.get_mention(as_html=True)}\n"
            f"Диалог: {message.chat.title}\n\n"
            f"{message.text}", parse_mode=ParseMode.HTML)
        await message.bot.send_message(
            message.chat.id,
            f"{message.from_user.get_mention(as_html=True)}, спасибо!\nТвой отчет сохранен.",
            parse_mode=ParseMode.HTML
        )
        await message.delete()
